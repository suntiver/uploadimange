
const Book =  require("../models/book");
const {validationResult}  = require('express-validator/check');



 /**
 *@suntiver  insertBook
 */
exports.insertBook = async (req,res,next) => {

            const  errors = validationResult(req);

            if(!errors.isEmpty()){
                 
                    return  res
                    .status(422).
                    json({message:'Validation failed,entered data is incorrect.',
                        errors:errors.array()
                    })

            }else{
                            const  createBook =  await Book.create({
                            name_book : req.body.name_book,
                            description	: req.body.description,
                            img_book : req.body.img_book,
                            price :req.body.price,
                            comment:req.body.comment,

                     });
                    return res.status(201).json({
                              message:`create a book  success!`,
                              success:true

                    })
            }
      




        } ;

 