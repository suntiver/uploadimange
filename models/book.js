const  Sequelize = require('sequelize');
const  sequelize = require('../config/database');

const Book  = sequelize.define('book',{

        id_book:{
                type:Sequelize.BIGINT(20),
                autoIncrement:true,
                allowNull:false,
                primaryKey:true
        },
        name_book:{
                type:Sequelize.STRING(255),
                allowNull:false

        },
        description:{
                type:Sequelize.STRING(255),
                allowNull:true

        },
        img_book:{
                type:Sequelize.STRING(255),
                allowNull:true
        },
        price:{
                type:Sequelize.FLOAT(8,2),
                allowNull:false
        },
        comment:{

                type:Sequelize.STRING(255),
                allowNull:true

        }

});


module.exports  = Book;