const  route  = require('express').Router();
const  bookController = require('../controllers/bookController');
const  {body}  = require('express-validator/check');



route.post("/insert-book",[
    body('name_book').not().isEmpty(),
    body('price').toFloat().not().isEmpty(),
],bookController.insertBook);





module.exports = route;